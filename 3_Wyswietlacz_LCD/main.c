/*
 * main.c
 *
 *  Created on: 28 lut 2018
 *      Author: dom
 */

#include <avr/io.h>
#include <inttypes.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include <avr/eeprom.h>

#include "lcd_44780/lcd_44780.h"

#define LED_PIN (1<<PD7)
#define LED_OFF PORTD &= ~LED_PIN
#define LED_ON PORTD |= LED_PIN


int main(void) {

	char hello[] = "hello";

	DDRC |= LED_PIN;
	LED_ON;
	_delay_ms(1000);
	LED_OFF;

	_Lcd_Init();
	_Lcd_Str(hello);

	while(1){

		PORTD ^= LED_PIN;
		_delay_ms(1000);

	}

	return 0;
}
