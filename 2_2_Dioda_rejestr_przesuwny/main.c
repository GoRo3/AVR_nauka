/*
 * Program do obslugi rjestru porzesownego SN74hc595 firmy Texas Instruments.
 *
 * Materiały:
 * 	- Rjestr przesuwny SN74hc595
 * 	-Attinny85
 * 	- Diody LED biale x8
 * 	Rezystory 300 Om.
 *
 * Źródła:
 *	- https://www.onsemi.com/pub/Collateral/MC74HC595-D.PDF
 *
 *  Created on: 24 lut 2018
 *  Author: GoRo3
 */

#include <avr/io.h>
#include <inttypes.h>
#include <util/delay.h>
#include <avr/interrupt.h>

#define MOSI_PIN (1<<PB1)		//PIN MOSI
#define CLK_PIN (1<<PB2) 		// PIN CLK
#define LT_PIN (1<<PB4)			//Latch PIN

#define LT_HIGH PORTB|=LT_PIN // Latch na stan wyoski
#define LT_LOW PORTB &=~LT_PIN // Latch na stan niski

uint8_t pierwsza[] = { 0, 1, 3, 6, 12, 24, 48, 96, 192, 128 };

uint8_t SpiTransfer(uint8_t data);

int main(void) {

	DDRB |= MOSI_PIN | CLK_PIN | LT_PIN; // Wejscie 0 i 2 w porcie B jako wyjscie.

	while (1) {
		for (int i = 0; i < sizeof(pierwsza); i++) {
			LT_LOW;
			SpiTransfer(~pierwsza[i]);
			LT_HIGH;
			_delay_ms(500);
		}
		for (int i = sizeof(pierwsza); i > 0; i--) {
			SpiTransfer(pierwsza[i]);
			_delay_ms(500);
		}
	}
	return 0;
}

// Funkcja do transferu danych po USI jako SPI.

uint8_t SpiTransfer(uint8_t data) {

	USIBR = data;
	USISR = _BV(USIOIF); // clear flag

	while (!(USISR & _BV(USIOIF))) {
		USICR = (1 << USIWM0) | (1 << USICS1) | (1 << USICLK) | (1 << USITC);
	}

	LT_LOW;
	LT_HIGH;
	return 1;
}

