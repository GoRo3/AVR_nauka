/*
 * main.c
 *
 *  Created on: 24 lut 2018
 *      Author: dom
 */

#include <avr/io.h>
#include <util/delay.h>

uint16_t pierwsza[] = {0,1,3,6,12,24,48,64};

int main(void){

	_delay_ms(5000);

	DDRB = 0xFF;
	PORTB = 0x00;

	_delay_ms(5000);

	while(1){

		for(int i=0; i <= sizeof(pierwsza); i++){
			PORTB = pierwsza[i];
			_delay_ms(250);
		}

		for(int i=sizeof(pierwsza); i >= 0 ; i--){
			PORTB = pierwsza[i];
			_delay_ms(250);
		}
	}

	return 0;
}
