/*****************************************************************
 * Nauka AVR. Postawy C, rozne programy. 
 * 
 * Dioda led podłączona pod ATTiny85 będzie sie zmieniac 
 * po wcisnieciu przycisku. Mikroswitch zwarty do masy.  
 * ***************************************************************/

#include <avr/io.h>
#include <util/delay.h>

//Dioda
#define LED_PIN (1 << PB1)       //Definicja pinu na ktorym jest podlaczona dioda.
#define LED_TOG PORTB ^= LED_PIN // Zmiana stanu diody
//Przycisk
#define KEY_PIN (1 << PB4)         //Przycisk pod portem PB4.
#define KEY_DOWN !(PINB & KEY_PIN) // Czy przcisk wcisniety

uint8_t stan_klawisza(void);

int main(void)
{

    /****** inicjalizacja *********/

    DDRB |= LED_PIN;  //kierunek pinu PB1 - wyjście
    PORTB |= LED_PIN; // Wylaczenie diody

    DDRB &= ~KEY_PIN; //Kierunek piny PB4 Wejciowy
    PORTB |= KEY_PIN; // Podciagniecie pinu do VCC przez rezystor wewnetrzny

    /*********** Petla glowna programu ******/

    while (1)
    {
        if (stan_klawisza()) //sprawdzamy czy funkcja zwrocila 1. 
        {
            LED_TOG;
            _delay_ms(200);
        }
    }
}

// Funkcja sprawdzajaca stan klawisza. Jesli jest wcisniety zwraca jeden. 
uint8_t stan_klawisza(void)
{
    if (KEY_DOWN)
    { // Czy klawisz wcisniety?
        _delay_ms(80);
        if (KEY_DOWN)
            return 1;
    }
    return 0; // Jesli klawisz nie wcisniety to wyjdzie z funkcji.
}