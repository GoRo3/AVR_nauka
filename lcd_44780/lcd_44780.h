/*
 * lcd_44780.c
 *
 *  Created on: 28 lut 2018
 *      Author: dom
 */

#ifndef LCD_44780_H_
#define LCD_44780_H_

// Biblioteka do sterowania wyświetlaczem LCD na bazie sterownika HD44780

/********************************** USER CONFIG ******************************/

//Definicja rozdzielczości wyświtlacza
#define LCD_Y 2     //liczba wierszy wyświetlacza
#define LCD_X 16    //Liczba znakow w wierszu

#define USE_LCD_LOCATE 1

//Ustawienie za pomoca 0 lub 1 czy sterujemy pinem R/W
#define USE_RW 1 // 0 - PIN R/W zwarty do GND, 1 - do UC

// Deklarowanie z ilu pinow korzystamy 4 lub 8.

#define USE_8_BIT 0 // 0 - 4 bity (D4-D7). 1 - 8 bitow (D0-D7)

// Konfiguracja portow i pinow podlaczenia D4 - D7
#define LCD_D7PORT A
#define LCD_D7 3
#define LCD_D6PORT A
#define LCD_D6 2
#define LCD_D5PORT A
#define LCD_D5 1
#define LCD_D4PORT A
#define LCD_D4 0

//Definicja portow/pinow procesora podlaczonych do R/W, R/S, E
#define LCD_RSPORT B
#define LCD_RS 2

#define LCD_RWPORT B
#define LCD_RW 1

#define LCD_EPORT B
#define LCD_E 0

/********************************** END OF CONFIG *****************************/
/********************************** COMAND DEFINICIONS *****************************/

#define LCDC_CLS					0x01
#define LCDC_HOME					0x02
#define LCDC_ENTRY					0x04
	#define LCDC_ENTRYR					0x02
	#define LCDC_ENTRYL					0
	#define LCDC_MOVE					0x01
#define LCDC_ONOFF					0x08
	#define LCDC_DISPLAYON				0x04
	#define LCDC_CURSORON				0x02
	#define LCDC_CURSOROFF				0
	#define LCDC_BLINKON				0x01
#define LCDC_SHIFT					0x10
	#define LCDC_SHIFTDISP				0x08
	#define LCDC_SHIFTR					0x04
	#define LCDC_SHIFTL					0
#define LCDC_FUNC					0x20
	#define LCDC_FUNC8B					0x10
	#define LCDC_FUNC4B					0
	#define LCDC_FUNC2L					0x08
	#define LCDC_FUNC1L					0
	#define LCDC_FUNC5x10				0x04
	#define LCDC_FUNC5x7				0
#define LCDC_SET_CGRAM				0x40
#define LCDC_SET_DDRAM				0x80


/********************************** LIBRARY DEFINICIONS *****************************/


// Makra upraszczaj�ce dost�p do port�w
// *** PORT
#define PORT(x) SPORT(x)
#define SPORT(x) (PORT##x)
// *** PIN
#define PIN(x) SPIN(x)
#define SPIN(x) (PIN##x)
// *** DDR
#define DDR(x) SDDR(x)
#define SDDR(x) (DDR##x)

//Adresy linii w zaleznosci od typu wyswietlacza.

#if ((LCD_Y == 4)&&(LCD_X == 20))
#define LCD_LINE1 0x00 //adress 1 znaku 1 wiersza
#define LCD_LINE2 0x28 //adres 1 znaku 2 wiersza
#define LCD_LINE3 0x14 //adres 1 anaku 3 wiersza
#define LCD_LINE4 0x54 //adress 1 znaku 4 wiersza
#else
#define LCD_LINE1 0x00 //adres 1 zaku 1 wiersza
#define LCD_LINE2 0x40 //adres 1 znaku 2 wiersza
#define LCD_LINE3 0x10 //adres 1 znaku 3 wiersza
#define LCD_LINE4 0x50 //adres 1 znaku 4 wiersza
#endif

/************************ Makrodefinicje Sterujące portami ************************/

// Definicje operacji na sygnalach sterujacych RS, RW, E

//Stan wyoski na linii RS
#define SET_RS PORT(LCD_RSPORT)|=(1<<LCD_RS)
//Stan niski na linii RS
#define CLR_RS PORT(LCD_RSPORT)&=~(1<<LCD_RS)
//Stan wyoski na linii RW - Odczyt z wyswietlacza
#define SET_RW PORT(LCD_RWPORT)|=(1<<LCD_RW)
// STAN NISKI NA LINII RW - ZAPIS DO WYSWITLACZA
#define CLR_RW PORT(LCD_RWPORT)&=~(1<<LCD_RW)
// SATAN wyoski na linii E
#define SET_E PORT(LCD_EPORT)|=(1<<LCD_E)
//STAN niski na linii E
#define CLR_E PORT(LCD_EPORT)&=~(1<<LCD_E)


/******************* Definicje Funkcji **********************************/

void lcd_init(void);
void lcd_cls(void);
void lcd_str(char *str);
void _lcdWrite_cmd(uint8_t cmd);
void _lcdWrite_data(uint8_t data);

#endif //LCD_44780_H_




