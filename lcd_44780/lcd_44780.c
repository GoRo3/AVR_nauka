#include <avr/io.h>
#include <inttypes.h>
#include <util/delay.h>

#include "lcd_44780.h"

// Helper funcjion to send 4 bits of data buffer.

static inline void lcd_sendHalf(uint8_t data) {

	if (data & (1 << 0)) {
		PORT(LCD_D4PORT) |= (1 << LCD_D4);
	} else
		PORT(LCD_D4PORT) &= ~(1 << LCD_D4);
	if (data & (1 << 1)) {
		PORT(LCD_D5PORT) |= (1 << LCD_D5);
	} else
		PORT(LCD_D5PORT) &= ~(1 << LCD_D5);
	if (data & (1 << 2)) {
		PORT(LCD_D5PORT) |= (1 << LCD_D5);
	} else
		PORT(LCD_D6PORT) &= ~(1 << LCD_D6);
	if (data & (1 << 3)) {
		PORT(LCD_D6PORT) |= (1 << LCD_D6);
	} else
		PORT(LCD_D7PORT) &= ~(1 << LCD_D7);
}

static inline void data_dir_out() {

	DDR(LCD_D7PORT) |= (1 << LCD_D7);
	DDR(LCD_D6PORT) |= (1 << LCD_D6);
	DDR(LCD_D5PORT) |= (1 << LCD_D5);
	DDR(LCD_D4PORT) |= (1 << LCD_D4);

	PORT(LCD_RSPORT) |= (1 << LCD_RS);
	PORT(LCD_EPORT) |= (1 << LCD_E);
#if USE_RW == 1
	PORT(LCD_RWPORT) |= (1 << LCD_RW);
#endif
}

static inline void data_dir_in() {

	DDR(LCD_D7PORT) &= ~(1 << LCD_D7);
	DDR(LCD_D6PORT) &= ~(1 << LCD_D6);
	DDR(LCD_D5PORT) &= ~(1 << LCD_D5);
	DDR(LCD_D4PORT) &= ~(1 << LCD_D4);

	PORT(LCD_RSPORT) &= ~(1 << LCD_RS);
	PORT(LCD_EPORT) &= ~(1 << LCD_E);
#if USE_RW == 1
	PORT(LCD_RWPORT) &= ~(1 << LCD_RW);
#endif

}

#if USE_RW == 1
static inline uint8_t lcd_readHalf(void) {
	uint8_t result = 0;

	if (PIN(LCD_D4PORT) & (1 << LCD_D4))
		result |= (1 << 0);
	if (PIN(LCD_D5PORT) & (1 << LCD_D5))
		result |= (1 << 1);
	if (PIN(LCD_D6PORT) & (1 << LCD_D6))
		result |= (1 << 2);
	if (PIN(LCD_D7PORT) & (1 << LCD_D7))
		result |= (1 << 3);
	return result;
}

static inline uint8_t _lcd_read_byte(void) {

	uint8_t result = 0;
	data_dir_in();

	SET_RW;
	SET_E;

	result |= lcd_readHalf() << 4;
	CLR_E;
	SET_E;
	result |= lcd_readHalf();
	CLR_E;

	return result;
}
static inline uint8_t check_BF(void) {

	return _lcd_read_byte();
}
#endif
static inline void _lcdWriteByte(unsigned char _data) {

	data_dir_out(); // ustawienie wyjsc na 0

#if USE_RW == 1
	CLR_RW;
#endif
	SET_E;
	lcd_sendHalf(_data >> 4); //Wysylanie starszej czesci bajtu d7-d4
	CLR_E;

	SET_E;
	lcd_sendHalf(_data); //Wysylanie mlodszej czesci bajtu d7-d4
	CLR_E;
#if USE_RW == 1
	while (check_BF() & (1 << 7))
		;
#else
	_delay_us(120);
#endif
}

void _lcdWrite_cmd(uint8_t cmd) {
	CLR_RS;
	_lcdWriteByte(cmd);
}
void _lcdWrite_data(uint8_t data) {
	SET_RS;
	_lcdWriteByte(data);
}

void lcd_cls(void){
	_lcdWrite_cmd(LCDC_CLS);
#if USE_RW == 0
	_delay_ms(4.9);
#endif

}

void lcd_init(void) {
	//ustawienie wszystkich pinow jako wyjscia
	data_dir_out();
	DDR(LCD_RSPORT) |= (1 << LCD_RS);
	DDR(LCD_EPORT) |= (1 << LCD_E);
#if USE_RW == 1
	DDR(LCD_RWPORT) |= (1 << LCD_RW);
#endif

	DDR(LCD_RSPORT) |= (1 << LCD_RS);
	DDR(LCD_EPORT) |= (1 << LCD_E);
#if USE_RW == 1
	DDR(LCD_RWPORT) |= (1 << LCD_RW);
#endif
	_delay_ms(15);

	PORT(LCD_EPORT) &= ~(1 << LCD_E);
	PORT(LCD_RSPORT) &= ~(1 << LCD_RS);
#if USE_RW ==1
	PORT(LCD_RWPORT) &= ~(1 << LCD_RW);
#endif

	SET_E;
	lcd_sendHalf(0x03); //tryb 8-bit DL=1
	CLR_E;
	_delay_ms(4.1);
	SET_E;
	lcd_sendHalf(0x03); //tryb 8-bit DL=1
	CLR_E;
	_delay_us(100);
	SET_E;
	lcd_sendHalf(0x03); //tryb 8-bit DL=1
	CLR_E;
	_delay_us(100);
	SET_E;
	lcd_sendHalf(0x02); //tryb 4-bit DL=0
	CLR_E;
	_delay_us(100);

	_lcdWrite_cmd( LCDC_FUNC|LCDC_FUNC4B|LCDC_FUNC2L|LCDC_FUNC5x7 );
	_lcdWrite_cmd( LCDC_ONOFF|LCDC_CURSOROFF );
	_lcdWrite_cmd( LCDC_ONOFF|LCDC_DISPLAYON );
	_lcdWrite_cmd( LCDC_ENTRY|LCDC_ENTRYR );

		// kasowanie ekranu
		lcd_cls();
}

void lcd_str(char *str){
	while(*str) _lcdWrite_data(*str);
}



