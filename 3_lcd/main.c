#include <avr/io.h>
#include <inttypes.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include <avr/eeprom.h>

#include "lcd_44780/lcd_44780.h"

#define LED_PIN (1<<PD7)
#define LED_OFF PORTD &= ~LED_PIN
#define LED_ON PORTD |= LED_PIN

int main(void) {
	int val = 1;
	char hello[] = "hello 123 hello";

	DDRC |= LED_PIN;
	LED_ON;
	_delay_ms(1000);
	LED_OFF;

	lcd_init();
	lcd_str(hello);

	while (1) {

		PORTD ^= LED_PIN;

		lcd_cls();
		lcd_locate(1, 0);
		lcd_hex(val);

		lcd_locate(0, 0);
		lcd_int(val);
		val += 1;

		_delay_ms(100);
	}

}
